# AIM - Augmented Interactive Management

### Purpose

This project's goal is the creation of augmented interactive management software.  

### Wiki

Read the project wiki to get a better idea about the goals, objectives and
features of this project.

### Issues

The issues log has the following issue templates:

1. Question

2. Comment
